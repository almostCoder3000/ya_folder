import {
    DiskActionTypes,
    DiskState,
    START_FETCH,
    FETCH_ERROR,
    GET_DISK_SUCCESS,
    GET_RESOURCES_SUCCESS
  } from "./types";
  
  const initialState: DiskState = {
    fetching: false,
    user: { login: "", display_name: "", uid: "", country: "" },
    total_space: 0, 
    used_space: 0, 
    resource_items: [],
    error: ""
  };
  
  export function diskReducer( 
      state = initialState, 
      action: DiskActionTypes
  ): DiskState {

      switch (action.type) {
          case START_FETCH:
              return {
                  ...state,
                  fetching: true
              };

          case GET_DISK_SUCCESS:
              return {
                  ...state,
                  ...action.payload,
                  fetching: false
              };

          case FETCH_ERROR:
              return {
                  ...state,
                  fetching: false,
                  error: action.error
              };

          
          case GET_RESOURCES_SUCCESS:              
              return {
                  ...state,
                  resource_items: action.payload,
                  fetching: false
              }

          default:
            return state;
      }
  }