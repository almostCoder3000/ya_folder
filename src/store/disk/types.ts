export interface IUser {
    login: string;
    display_name: string;
    uid: string;
    country: string;
}

export interface IResourceItem {
    name: string;
    type: string;
    path: string;
    public_url: string;
    mime_type: string;
}

export interface IDisk {
    total_space: number;
    used_space: number;
    resource_items: IResourceItem[];
}

export interface DiskState {
    fetching: boolean;
    user: IUser;
    total_space: number;
    used_space: number;
    resource_items: IResourceItem[];
    error: any | undefined;
}


export const START_FETCH = "START_FETCH";
export const GET_DISK_SUCCESS = "GET_DISK_SUCCESS";
export const FETCH_ERROR = "FETCH_ERROR";

export const GET_RESOURCES_SUCCESS = "GET_RESOURCES_SUCCESS";


export interface StartFetchAction {
    type: typeof START_FETCH;
    payload?: DiskState
}

interface FetchActionError {
    type: typeof FETCH_ERROR;
    payload?: DiskState;
    error?: any;
}


interface GetDiskActionSuccess {
    type: typeof GET_DISK_SUCCESS;
    payload: DiskState;
}

interface GetResourceSuccess {
    type: typeof GET_RESOURCES_SUCCESS;
    payload: IResourceItem[]
}


export type DiskActionTypes = StartFetchAction | 
                              GetDiskActionSuccess |
                              GetResourceSuccess |
                              FetchActionError;