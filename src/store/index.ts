import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { diskReducer } from "./disk/reducers";

const rootReducer = combineReducers({
  disk: diskReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  const middlewares = [thunkMiddleware];
  const middleWareEnhancer = applyMiddleware(...middlewares);

  const store = createStore(
    rootReducer,
    composeWithDevTools(middleWareEnhancer)
  );

  if (module.hot) {
    module.hot.accept('./', () => store.replaceReducer(rootReducer))
  }

  return store;
}
